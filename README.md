# Java-Serenity-Cucumber Weatherstack API Test Automation
[![GitLab](https://gitlab.com/merve_asker/java-serenity-cucumber-weatherstack-api-test/uploads/0abdcffd7b38ae848852d328083ada49/GitLab.png)](https://gitlab.com/merve_asker/java-serenity-cucumber-weatherstack-api-test)

# Introduction

This is a Weatherstack Rest API test solution for sample endpoints available in https://weatherstack.com/. The published APIs represent a weather broadcast application where users can asks for real-time and historical weather information.

Tests are written using a combination of SerenityBDD, RestAssured, Cucumber, Junit & Maven.

# The project directory structure

```Gherkin
src
  + test
    + java                                              Test runners and supporting code
      + common                                          Package for all common actions
          CommonRequestSpec                             Common Request Spec for the API calls
      + run
          CucumberTestSuite                             Cucumber configurations and options
      + stepdefinitions                                 Step definitions for the BDD features          					
      + steps                                           Domain model package consisting of all actions to get weatherstack functionality
          WeatherstackGetActions                        API calls on Weatherstack APIs
    + resources
      + features                                        Feature files directory
          current_weatherstack_api_test.feature         Feature containing BDD scenarios
          historical_weatherstack_api_test.feature      Feature containing BDD scenarios
      + schema                                          Folder containing json schema for API schema validation
      serenity.conf                                     Configurations file

```
# Executing the tests
Run `mvn clean verify` from the command line.

The test results will be recorded here `target/site/serenity/index.html`.
Please run the below command from root directory to open the result after execution.
```bash
open target/site/serenity/index.html 
```
Reports can be seen in GitLab, under the artifacts section `serenity/index.html`.
Here is the direct link to the GitLab project page: https://gitlab.com/merve_asker/java-serenity-cucumber-weatherstack-api-test
Here is the direct link to the GitLab project pipeline: https://gitlab.com/merve_asker/java-serenity-cucumber-weatherstack-api-test/-/pipelines

The report records the API calls and its response in a very readable format as shown below.
![serenityBDD_report_sample](https://gitlab.com/merve_asker/java-serenity-cucumber-weatherstack-api-test/uploads/963d53cb2b77e98b084ec5f857cc2cd3/serenityBDD_report_sample.png)

# Additional configurations

Additional command line parameters can be passed for switching the application environment.
```json
$ mvn clean verify -Denvironment=dev
```
Configurations to for different environments are set in the `test/resources/serenity.conf` file. In real time projects each environment can be configured with its baseurl to run the tests based on different environments.
```
environments {
  default {
    baseurl = "http://api.weatherstack.com"
  }
  dev {
    baseurl = "http://api.weatherstack.com"
  }
  staging {
    baseurl = "http://api.weatherstack.com"
  }
}
```
