Feature: Getting Weatherstack Api's Current End Point results

  Scenario: Get a valid city's current weather forecast
    When I call endpoint with "Istanbul" city and "valid" access key for getting "current" weather forecast
    Then Response should be retrieved with code 200
    And The schema should match with the specification defined in "response_for_valid_city.json"
    And "location.name" field's value should be "Istanbul"
    And "location.country" field's value should be "Turkey"

  Scenario Outline: Failure response should be returned while using invalid query parameters
    When I call endpoint with "<city>" city and "<accessKey>" access key for getting "current" weather forecast
    Then Response should be retrieved with code 200
    And The schema should match with the specification defined in "response_for_invalid_call.json"
    And "success" field's value should be "false"
    And "error.code" field's value should be "<error_code>"
    And "error.type" field's value should be "<errorType>"
    And "error.info" field's value should be "<errorMessage>"
    Examples:
      | city      | accessKey | error_code | errorType          | errorMessage                                                                            |
      | Istanbull | valid     | 615        | request_failed     | Your API request failed. Please try again or contact support.                           |
      | Istanbul  | invalid   | 101        | invalid_access_key | You have not supplied a valid API Access Key. [Technical Support: support@apilayer.com] |

  Scenario Outline: Failure response should be returned while there is missing query parameters
    When I call "current" weather forecast endpoint with one "<parameter>" parameter and "<parameterValue>" value
    Then Response should be retrieved with code 200
    And The schema should match with the specification defined in "response_for_invalid_call.json"
    And "success" field's value should be "false"
    And "error.code" field's value should be "<error_code>"
    And "error.type" field's value should be "<errorType>"
    And "error.info" field's value should be "<errorMessage>"
    Examples:
      | parameter  | parameterValue                   | error_code | errorType          | errorMessage                                                                           |
      | access_key | 9fb75aabb2efd1b7ebf78a44867a42b7 | 601        | missing_query      | Please specify a valid location identifier using the query parameter.                  |
      | query      | Istanbul                         | 101        | missing_access_key | You have not supplied an API Access Key. [Required format: access_key=YOUR_ACCESS_KEY] |

  Scenario: Failure response should be returned while there is no query parameters
    When I call "current" weather forecast endpoint with no query parameter
    Then Response should be retrieved with code 200
    And The schema should match with the specification defined in "response_for_invalid_call.json"
    And "success" field's value should be "false"
    And "error.code" field's value should be "101"
    And "error.type" field's value should be "missing_access_key"
    And "error.info" field's value should be "You have not supplied an API Access Key. [Required format: access_key=YOUR_ACCESS_KEY]"
