package com.api.steps;

import io.restassured.response.Response;

import com.api.common.CommonRequestSpec;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static org.assertj.core.api.Assertions.assertThat;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;


public class WeatherstackGetActions {

    private Response response;
    private String accessKey;

    @Step("Response should be retrieved with code {integer}")
    public void verifyResponseCode(int responseCode, Response lastResponse) {
        assertThat(lastResponse.statusCode()).isEqualTo(responseCode);
    }

    @Step("The schema should match with the specification defined in {string}")
    public void verifyResponseSchema(Response lastResponse, String schemaJson) {
        lastResponse.then().body(matchesJsonSchemaInClasspath("schema/" + schemaJson));
    }

    @Step("Call endpoint with {string} city and {string} access key for getting {string} weather forecast")
    public Response sendGetRequestForCityWeatherForecast(String cityName, String accessKey, String forecastType) {
        if (accessKey.equals("valid")) {
            accessKey = "9fb75aabb2efd1b7ebf78a44867a42b7";
        } else {
            accessKey = "9fb75aabb2efd1b7ebf78a44867a4mmm";
        }

        response = SerenityRest.given().header("Content-Type", "application/json")
                .spec(CommonRequestSpec.commonReqSpec()).basePath("/{name}").pathParam("name", forecastType)
                .queryParam("access_key", accessKey)
                .queryParam("query", cityName)
                .get().then().extract().response();
        response.prettyPrint();
        System.out.println(response);
        return response;
    }

    @Step("Get field value from response")
    public String getFieldValue(String field, Response weatherDetailResp) {
        return (weatherDetailResp.getBody().jsonPath().getString(field));
    }

    @Step("{string} field's value should be {string}")
    public void validateFieldValue(String field, String fieldValue, Response lastResponse) {
        assertThat(getFieldValue(field, lastResponse)).isEqualToIgnoringCase(fieldValue);
    }

    @Step("Verify that response is empty list")
    public void responseShouldBeEmptyList(Response lastResponse) {
        assertThat(lastResponse.getBody().jsonPath().getList("").size()).isEqualTo(0);
    }

    @Step("Call {string} weather forecast endpoint with only {string} parameter and {string} value")
    public Response sendGetRequestForOneParamValuePair(String forecastType, String parameter, String value) {
        response = SerenityRest.given().header("Content-Type", "application/json")
                .spec(CommonRequestSpec.commonReqSpec()).basePath("/{name}").pathParam("name", forecastType)
                .queryParam(parameter, value)
                .get().then().extract().response();
        response.prettyPrint();
        System.out.println(response);
        return response;
    }

    @Step("Call endpoint for getting {string} city and {string} access key for {string} weather forecast in {string}")
    public Response sendGetRequestForHistoricalCityWeatherForecast(String cityName, String accessKey, String forecastType, String date) {

        if (accessKey.equals("valid")) {
            accessKey = "9fb75aabb2efd1b7ebf78a44867a42b7";
        } else {
            accessKey = "9fb75aabb2efd1b7ebf78a44867a4mmm";
        }
        response = SerenityRest.given().header("Content-Type", "application/json")
                .spec(CommonRequestSpec.commonReqSpec()).basePath("/{name}").pathParam("name", forecastType)
                .queryParam("access_key", accessKey)
                .queryParam("query", cityName)
                .queryParam("historical_date", date)
                .get().then().extract().response();
        response.prettyPrint();
        System.out.println(response);
        return response;
    }

    @Step("I call {string} weather forecast endpoint with no parameter")
    public Response sendGetRequestWithNoQueryParameter(String forecastType) {
        response = SerenityRest.given().header("Content-Type", "application/json")
                .spec(CommonRequestSpec.commonReqSpec()).basePath("/{name}").pathParam("name", forecastType)
                .get().then().extract().response();
        response.prettyPrint();
        System.out.println(response);
        return response;
    }

}
