package com.api.stepdefinitions;

import com.api.steps.WeatherstackGetActions;
import static net.serenitybdd.rest.SerenityRest.lastResponse;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class GetWeatherSteps {

	@Steps
	WeatherstackGetActions steps;

	@And("The schema should match with the specification defined in {string}")
	public void theSchemaShouldMatchWithTheSpecificationDefinedIn(String spec) {
		steps.verifyResponseSchema(lastResponse(), spec);
	}

	@When("I call endpoint with {string} city and {string} access key for getting {string} weather forecast")
	public void callEndpointForGettingCityWeatherForecast(String cityName, String accessKey, String forecastType) {
		steps.sendGetRequestForCityWeatherForecast(cityName, accessKey, forecastType);
	}

	@Then("Response should be retrieved with code {int}")
	public void responseShouldBeRetrievedWithCode(Integer code) {
		steps.verifyResponseCode(code, lastResponse());
	}

	@And("{string} field's value should be {string}")
	public void fieldSShouldBe(String field, String fieldValue) {
		steps.validateFieldValue(field, fieldValue, lastResponse());
	}

	@Then("Empty response should be returned")
	public void emptyResponseShouldBeReturned() {
		steps.responseShouldBeEmptyList(lastResponse());
	}

	@When("I call {string} weather forecast endpoint with one {string} parameter and {string} value")
	public void callWeatherForecastEndpointWithOnlyParameterAndValue(String forecastType, String parameter, String value) {
		steps.sendGetRequestForOneParamValuePair(forecastType, parameter, value );
	}

	@When("I call endpoint for getting {string} city and {string} access key for {string} weather forecast in {string}")
	public void callEndpointForGettingCityWeatherForecastIn(String cityName, String accessKey, String forecastType, String date) {
		steps.sendGetRequestForHistoricalCityWeatherForecast(cityName, accessKey, forecastType, date);
	}

	@When("I call {string} weather forecast endpoint with no query parameter")
	public void callWeatherForecastEndpointWithNoParameter(String forecastType) {
		steps.sendGetRequestWithNoQueryParameter(forecastType);
	}

}
